#!/bin/env bash

echo "run warp"
warp-cli connect

echo "filter malware"
warp-cli set-families-mode malware

echo "verifing"
curl https://www.cloudflare.com/cdn-cgi/trace/

echo "to set always on: warp-cli enable-always-on"
