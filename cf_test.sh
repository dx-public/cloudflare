#!/bin/env bash

echo
echo "testing"
echo

echo "if returns doh is enabled"
dig +cd +short -p 53 is-doh.cloudflareresolve.com
echo
echo "if returns dot is enabled"
dig +cd +short -p 53 is-dot.cloudflareresolve.com
echo
echo "other useful info"
curl https://www.cloudflare.com/cdn-cgi/trace/ 
