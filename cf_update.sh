#!/bin/env bash

#echo "updating cloudflared"
#https://pkg.cloudflareclient.com/uploads/cloudflare_warp_2022_2_29_1_amd64_2b22275edd.deb
#https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-amd64
#https://github.com/ViRb3/wgcf/releases/download/v2.2.11/wgcf_2.2.11_linux_amd64

echo "get key"
curl https://pkg.cloudflareclient.com/pubkey.gpg | sudo gpg --yes --dearmor --output /usr/share/keyrings/cloudflare-warp-archive-keyring.gpg

echo "sources.list"
echo 'deb [arch=amd64 signed-by=/usr/share/keyrings/cloudflare-warp-archive-keyring.gpg] https://pkg.cloudflareclient.com/ bullseye main' | sudo tee /etc/apt/sources.list.d/cloudflare-client.list

echo "update"
sudo apt update

echo "instaling warp"
apt install -y cloudflare-warp

echo "updating warp"
apt upgrade -y cloudflare-warp

echo "register warp"
warp-cli register
