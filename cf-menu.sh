#!/bin/env bash

#get current dir
cdir=`dirname $0`

#check status
[ $(git rev-parse HEAD) = $(git ls-remote $(git rev-parse --abbrev-ref @{u} | sed 's/\// /g') | cut -f1) ] && changed=0 || changed=1
if [ $changed = 1 ]; then
 #update git-repo first
 #git reset -- hard # for resetting
 echo "Updating script.."
 git -C $cdir pull
 echo "Please rerun cf-menu.sh"
 exit
fi

ls /root/ 1>/dev/null 2>&1 || (echo "Please run as root" && exit)

# Running a forever loop using while statement
# This loop will run untill select the exit option.
# User will be asked to select option again and again
while :
do

# creating a menu with the following options
echo "--------------"
echo "Cloudflare Menu"
echo "--------------"
echo "1.  Update cloudflared"
echo "2.  Run cloudflared"
echo "3.  Test cloudflare"
echo -n "98. Reboot"; [ -e /var/run/reboot-required ] && echo " (NEEDED) " || echo ""
echo "99. Exit menu "
echo -n "Enter your menu choice [#]: "

# reading choice
read choice

# case statement is used to compare one value with the multiple cases.
case $choice in
  # Pattern 1
  1)  echo "You have selected the option 1"
      echo "Updating cloudflared... "
      sh $cdir/cf_update.sh;;
  # Pattern 2
  2)  echo "You have selected the option 2"
      echo "Running vpn script... cf_vpnservice.sh "
      sh $cdir/cf_vpnservice.sh;;
  # Pattern 3
  3)  echo "You have selected the option 3"
      echo "Running vpn script... cf_test.sh "
      sh $cdir/cf_test.sh;;
  # Pattern 98
  98|r)  echo "Rebooting CF Service ..."
      systemctl status cloudflared;;
  # Pattern 99
  99|q|'')  echo "Quitting ..."
       exit;;
  # Default Pattern
  *) echo "" && echo "invalid option" && echo "";;

esac
  #echo -n "Enter your menu choice [#]: "
done
